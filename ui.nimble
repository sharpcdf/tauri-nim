# Package

version       = "0.1.0"
author        = "sharpcdf"
description   = "A new awesome nimble package"
license       = "MIT"
srcDir        = "ui"
# Dependencies

requires "nim >= 1.6.10"

task dev, "compiles code to js and runs in development mode":
    exec "nimble js ui/ui.nim --out:index.js --outdir:ui --debug --verbose"
    exec "cargo tauri dev"

task release, "builds and bundles everything":
    exec "nimble js ui/ui.nim --out:index.js --outdir:ui --silent"
    echo "finished building nim files"
    exec "cargo tauri build"
    echo "builds are in src-tauri/target/release"